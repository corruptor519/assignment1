var db;

function defaultEmail() {
    localStorage.setItem("DefaultEmail","kngo2269@conestogac.on.ca");
    $("#KNDefaultReviewer").attr("value", "kngo2269@conestogac.on.ca");
}

function errorHandler(tx, error) {
    console.error("SQL Error: " + tx + " ( " + error.code + ") -- " + error.message);
}

var DB = {
    KNcreateDatabase: function(){
        var shortName = "KNFeedbackDB";
        var version = "1.0";
        var displayName = "DB for KNFeedback app";
        var dbSize = 2 * 1024 * 1024;

        function dbCreateSuccess(){
            console.info("Success: Database creation successful");
        }
        db = openDatabase(shortName, version, displayName, dbSize, dbCreateSuccess);

    },
    KNcreateTables: function () {
        function txFunction(tx){

            var sqlDropTables = "DROP TABLE IF EXISTS type;";
            var sqlCreateTables = "CREATE TABLE IF NOT EXISTS type(" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "name VARCHAR(20) NOT NULL);";
            var options = [];
            function successCallback(){
                console.info("Success: Table creation successful");
            }

            tx.executeSql(sqlDropTables, options, successCallback, errorHandler);
            tx.executeSql(sqlCreateTables, options, successCallback, errorHandler);
            var sqlFirstInsert = "INSERT INTO type(name) VALUES('Other');";
            tx.executeSql(sqlFirstInsert, options, successCallback, errorHandler);
            var sqlSecondInsert = "INSERT INTO type(name) VALUES('Asian');";
            tx.executeSql(sqlSecondInsert, options, successCallback, errorHandler);
            var sqlThirdInsert = "INSERT INTO type(name) VALUES('Canadian');";
            tx.executeSql(sqlThirdInsert, options, successCallback, errorHandler);

            var sqlReviewTable = "CREATE TABLE IF NOT EXISTS review(" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "businessName VARCHAR(30) NOT NULL," +
                "typeId INTEGER NOT NULL," +
                "reviewerEmail VARCHAR (30)," +
                "reviewerComments TEXT," +
                "reviewDate DATE," +
                "hasRating VARCHAR (1)," +
                "rating1 INTEGER," +
                "rating2 INTEGER," +
                "rating3 INTEGER," +
                "FOREIGN KEY(typeId) REFERENCES type (id));";

            tx.executeSql(sqlReviewTable, options, successCallback, errorHandler);
        }
        function successCallback(){
            console.info("Success: Table creation successful");
        }

        db.transaction(txFunction, errorHandler, errorHandler);
    },
    KNDropTables: function(){
        function txFunction(tx){
            var sqlDropTableExist = "DROP TABLE IF EXISTS review; ";
            var sqlDropTablesType = "DROP TABLE IF EXISTS type;";
            var options = [];
            function successCallback(){
                console.info("Success: Table dropped successfully");
            }

            tx.executeSql(sqlDropTableExist, sqlDropTablesType, options, successCallback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    }
};