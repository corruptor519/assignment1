var review = {
    KNinsert: function(options, callback){
        function txFunction(tx){
            var sql="INSERT INTO review(businessName, typeId, reviewerEmail, reviewerComments, reviewDate, hasRating,foodRating, serviceRating, valueRating) values(?,?,?,?,?,?,?,?,?);";
            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    KNselectAll: function (options, callback){
        function txFunction(tx){
            var sql="SELECT * FROM review;";
            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    KNselect: function (options, callback){
        function txFunction(tx){
            var sql="SELECT * FROM review WHERE id=?;";
            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    KNupdate: function (options, callback){
        function txFunction(tx){
            var sql="UPDATE friend SET name=?, fullName=?, dob=?, isFriend=? WHERE id=?;";
            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    KNdelete: function (options, callback){
        function txFunction(tx){
            var sql="DELETE FROM review WHERE id=?;";
            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    }
};

var type = {
    KNselectAll: function (options, callback){
        function txFunction(tx){
            var sql="SELECT * FROM type;";
            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction(){
            console.info("Success: Transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
}