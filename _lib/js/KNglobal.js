function btnSave1_click() {
    checkValidation();
    KNaddFeedback();
}

function btnUpdate_click()
{
    checkValidationEdit();
    KNupdateFeedback();
}
function btnSaveDefaultEmail_click() {
    saveDefaultEmail();
}

function KNchkAddRatings_click() {
    if ($("#KNchkAddRatings").is(":checked")) {
        $("#KNchkRating").show();
    }
    else
    {
        $("#KNchkRating").hide();
    }
}
function KNchkAddRatings1_click() {
    if ($("#KNchkAddRatings1").is(":checked")) {
        $("#KNchkRating1").show();
    }
    else
    {
        $("#KNchkRating1").hide();
    }
}

function KNchkAddRatings2_click() {
    if ($("#KNchkAddRatings2").is(":checked")) {
        $("#KNchkRating2").show();
    }
    else
    {
        $("#KNchkRating2").hide();
    }
}


function initLocalStorage() {
    defaultEmail();
}

function KNFoodQuality_changed()
{
    calculateRatingsAdd();
}

function KNService_changed()
{
    calculateRatingsAdd();
}

function KNValue_changed()
{
    calculateRatingsAdd();
}

function pageAdd_show() {
    defaultAddEmail();
    KNupdateTypesDropDown();
}
function viewFeedbackPage_show() {
    KNgetReviews();
}
function editFeedbackPage_show() {
    KNupdateReviewTypesDropDown();
    KNshowCurrentReview();
}

function btnClearDatabase_click() {
    KNClearDatabase();
    initDB();
}

function btnDelete1_click() {
    KNDeleteFeedback();
}

function init() {
    console.info("DOM is ready");
    $("#btnSave1").on("click", btnSave1_click);
    $("#btnUpdate1").on("click", btnUpdate_click);
    $("#btnSaveDefaultEmail").on("click", btnSaveDefaultEmail_click);
    $("#KNFoodQuality").on("change", KNFoodQuality_changed);
    $("#KNService").on("change", KNService_changed);
    $("#KNValue").on("change", KNValue_changed);
    $("#KNchkRating").hide();
    $("#KNchkRating1").hide();
    $("#KNchkRating2").hide();
    $("#KNchkAddRatings").on("click", KNchkAddRatings_click);
    $("#KNchkAddRatings1").on("click", KNchkAddRatings1_click);
    $("#KNchkAddRatings2").on("click", KNchkAddRatings2_click);
    $("#btnDelete1").on("click", btnDelete1_click);
    $("#btnClearDatabase").on("click", btnClearDatabase_click);
    $("#KNAddFeedbackPage").on("pageshow",pageAdd_show);
    $("#KNEditFeedbackPage1").on("pageshow",editFeedbackPage_show);
    $("#KNViewFeedbackPage").on("pageshow",viewFeedbackPage_show);
}

function initDB(){
    console.info("Creating Database... ");
    try {
        DB.KNcreateDatabase();
        if (db) {
            console.info("Creating Tables...");
            DB.KNcreateTables();
        }
        else{
            console.error("Error: cannot create tables: Database is not available");
        }
    } catch (e) {
        console.error("Error:  (Fatal) Error in initDB(). Cannot proceed");
    }

}



$(document).ready(function () {
    init();
    initDB();
    initLocalStorage();
});
