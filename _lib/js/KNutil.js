jQuery.validator.addMethod("checkRange",
    function (value, element) {
        if (value >= 0 && value <= 5)
        {
            return true;
        }
        return false;

    },
    "Our Range value checker"
);

//email validator for conestoga email
jQuery.validator.addMethod( "emailCheck",
    function (value, element) {
        var regex = /^.+@conestogac.on.ca$/;

        return this.optional(element) || regex.test(value);
    },
    "Our custom email checker"
);

function checkValidation() {
    if (doValidate_frmKNBusinessAdd()) {
        console.info("Form is valid");
    }
    else{
        console.error("Form is invalid");
    }
}

function checkValidationEdit() {
    if (doValidate_frmKNBusinessEdit1()) {
        console.info("Form is valid");
    }
    else{
        console.error("Form is invalid");
    }
}

function saveDefaultEmail()
{
    var email = $("#KNDefaultReviewerEmail").val();
    alert("Default reviewer email saved");
    localStorage.setItem("DefaultEmail", email);
}

function calculateRatingsAdd()
{
    var KNFoodQuality = $("#KNFoodQuality").val();
    var KNServices = $("#KNService").val();
    var KNValue = $("#KNValue").val();
    var overallRating = (parseInt (KNFoodQuality)  + parseInt(KNServices)+ parseInt (KNValue) )* 100/15;
    $("#KNOverallRating").val(overallRating +"%");
}

//Add page validation
function doValidate_frmKNBusinessAdd() {
    var form = $("#frmKNBusinessAdd");
    form.validate({
        rules:{
            KNBusinessName:{
                required: true,
                rangelength: [2,20]
            },
            KNReviewerEmail:{
                required: true,
                emailCheck: true
            },
            KNtxtReviewDate:{
                required: true,
                email: true
            },
            KNFoodQuality:{
                required: true,
                checkRange: true
            },
            KNService:{
                required: true,
                checkRange: true
            },
            KNValue:{
                required: true,
                checkRange: true
            }
        },
        messages:{
            KNBusinessName:{
                required: "Business name is required",
                rangelength: "Length must be 2-20 characters long"
            },
            KNReviewerEmail: {
                required: "Please enter a valid email address",
                emailCheck: "Email must be a conestoga email"
            },
            KNtxtReviewDate:{
                required: "Review date is required"
            },
            KNFoodQuality: {
                required: "Food quality value is required",
                checkRange: "Value must be 0-5"
            },
            KNService: {
                required: "Service is required",
                checkRange: "Value must be 0-5"
            },
            KNValue: {
                required: "Value is required",
                checkRange: "Value must be 0-5"
            }
        }
    });
    return form.valid();
}
//Edit/Modify Page validation
function doValidate_frmKNBusinessEdit1() {
    var form = $("#frmKNBusinessEdit1");
    form.validate({
        rules:{
            KNBusinessName1:{
                required: true,
                rangelength: [2,20]
            },
            KNReviewerEmail1:{
                required: true,
                emailCheck: true
            },
            KNtxtReviewDate1:{
                required: true,
                email: true
            },
            KNFoodQuality1:{
                required: true,
                checkRange: true
            },
            KNService1:{
                required: true,
                checkRange: true
            },
            KNValue1:{
                required: true,
                checkRange: true
            }
        },
        messages:{
            KNBusinessName1:{
                required: "Business name is required",
                rangelength: "Length must be 2-20 characters long"
            },
            KNReviewerEmail1: {
                required: "Please enter a valid email address",
                emailCheck: "Email must be a conestoga email"
            },
            KNtxtReviewDate1:{
                required: "Review date is required"
            },
            KNFoodQuality1: {
                required: "Food quality value is required",
                checkRange: "Value must be 0-5"
            },
            KNService1: {
                required: "Service is required",
                checkRange: "Value must be 0-5"
            },
            KNValue1: {
                required: "Value is required",
                checkRange: "Value must be 0-5"
            }
        }
    });
    return form.valid();
}