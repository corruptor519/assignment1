function KNupdateTypesDropDown() {
    var options = [];
    function callback(tx, results){

        var htmlCode = "";
        for (var i = 0; i < results.rows.length; i++){
            var row = results.rows.item(i);
            console.info("id: " + row['id'] +
                " name: " + row['name']
            );
            htmlCode += "<option value=" + row['id'] + ">" +
                row['name'] + "</option>";
        }
        var dropDown = $("#cmbType");
        dropDown = dropDown.html(htmlCode);
        dropDown.selectmenu("refresh");
    }
    type.KNselectAll(options, callback);
}

function KNupdateReviewTypesDropDown() {
    var options = [];
    function callback(tx, results){

        var htmlCode = "";
        for (var i = 0; i < results.rows.length; i++){
            var row = results.rows.item(i);
            console.info("id: " + row['id'] +
                " name: " + row['name']
            );
            htmlCode += "<option value=" + row['id'] + ">" +
                row['name'] + "</option>";
        }
        var dropDown = $("#cmbTypeEdit1");
        dropDown = dropDown.html(htmlCode);
        dropDown.selectmenu("refresh");
    }
    type.KNselectAll(options, callback);
}
function KNaddFeedback() {
    if (doValidate_frmKNBusinessAdd()) {
        console.info("Form is valid");

        var businessName = $("#KNBusinessName").val();
        var typeId = $("#cmbType").val();
        var reviewerEmail = $("#KNReviewerEmail").val();
        var reviewerComments= $("#KNReviewerComments").val();
        var reviewDate = $("#KNtxtReviewDate").val();
        var hasRating = $("#KNchkAddRatings").prop("checked");
        //save rating values if rating is checked
        if (hasRating == true) {
            var foodRating = $("#KNFoodQuality").val();
            var serviceRating = $("#KNService").val();
            var valueRating = $("#KNValue").val();
        }
        else{
            var foodRating = null;
            var serviceRating = null;
            var valueRating = null;
        }
        var options = [businessName, typeId, reviewerEmail, reviewerComments, reviewDate, hasRating, foodRating, serviceRating, valueRating];
        function callback(){
            alert("New Feedback Added");
        }
        review.KNinsert(options, callback);
    }
    else{
        console.error("Form is invalid");
    }
}

function KNgetReviews() {
    var options = [];
    function callback(tx, results){

        var htmlCode = "";
        for (var i = 0; i < results.rows.length; i++){
            var row = results.rows.item(i);
            console.info("id: " + row['id'] +
                " businessName: " + row['businessName'] +
                " typeId: " + row['typeId'] +
                " reviewerEmail: " + row['reviewerEmail'] +
                " reviewerComments: " + row['reviewerComments'] +
                " reviewDate: " + row['reviewDate'] +
                " hasRating: " + row['hasRating'] +
                " foodRating: " + row['foodRating'] +
                " serviceRating: " + row['serviceRating'] +
                " valueRating: " + row['valueRating']
            );
            // calculate average ratings
            var total = Math.round((row['foodRating'] + row['serviceRating'] + row['valueRating']) * 100 / 15);
            htmlCode += "<li><a data-role='button' data-row-id=" + row['id'] + " href='#' >" +
                "<h1>Business Name: " + row['businessName'] + "</h1>" +
                "<p>Email: " + row['reviewerEmail'] + "</p>" +
                "<p>Comments: " + row['reviewerComments'] + "</p>" +
                "<p>Overall Rating: " + total + "</p>" +
                "</a></li>";
        }
        var lv = $("#KNlvReview");
        lv = lv.html(htmlCode);
        lv.listview("refresh");

        $("#KNlvReview a").on("click",clickHandler);
        function clickHandler() {
            var id = $(this).attr("data-row-id");
            localStorage.setItem("id", id);
            $(location).prop("href","#KNEditFeedbackPage1");
        }
    }
    review.KNselectAll(options, callback)
}

function KNshowCurrentReview() {

    var id = localStorage.getItem("id");
    var options = [id];
    function callback(tx, results){
        var row = results.rows.item(0);
        var rating = row['hasRating'];
        var dropDown = $("#cmbTypeEdit1");
        console.info("id: " + row['id'] + " rating: " + rating );

        $("#KNBusinessName1").val(row['businessName']);
        $("#cmbTypeEdit1").find('option:selected').val(row['typeId']);
        $("#KNReviewerEmail1").val(row['reviewerEmail']);
        $("#KNReviewerComments1").val(row['reviewerComments']);
        $("#KNtxtReviewDate1").val(row['reviewDate']);
        if (rating === 'true'){
            $("#KNchkAddRatings1").prop("checked",true);
            $("#KNchkRating1").show();
        }
        else{
            $("#KNchkAddRatings1").prop("checked",false);
            $("#KNchkRating1").hide();
        }
        $("#KNFoodQuality1").val(row['foodRating']);
        $("#KNService1").val(row['serviceRating']);
        $("#KNValue1").val(row['valueRating']);
        $("#frmKNBusinessEdit1 :checkbox").checkboxradio("refresh");
        dropDown.selectmenu("refresh");
    }

    review.KNselect(options, callback);
}

function KNupdateFeedback() {
    var id = localStorage.getItem("id");
    var businessName = $("#KNBusinessName1").val();
    var typeId = $("#cmbTypeEdit1").val();
    var reviewerEmail = $("#KNReviewerEmail1").val();
    var reviewerComments = $("#KNReviewerComments1").val();
    var reviewDate = $("#KNtxtReviewDate1").val();
    var hasRating = $("#KNchkAddRatings1").prop("checked");
    var foodRating = $("#KNFoodQuality1").val();
    var serviceRating = $("#KNService1").val();
    var valueRating = $("#KNValue1").val();

    var options = [businessName, typeId, reviewerEmail, reviewerComments, reviewDate, hasRating, foodRating, serviceRating, valueRating, id];
    function callback(){
        console.info("Record Updated Successfully");
        alert("Feedback Updated Successfully");

    }
    review.KNupdate(options,callback);
    $(location).prop("href","#KNViewFeedbackPage");
}

function KNDeleteFeedback() {
    var id = localStorage.getItem("id");
    var options = [id];
    function callback(){
        console.info("Feedback Deleted Successfully");
        alert("Feedback Deleted Successfully");
    }
    review.KNdelete(options,callback);
    $(location).prop("href","#KNViewFeedbackPage");

}

function defaultAddEmail() {
    var email = localStorage.getItem("DefaultEmail")
    $("#KNReviewerEmail").attr("value", email);
}

function KNClearDatabase() {
    var result = confirm("Really want to clear the database?");
    try  {
        if (result){
            DB.KNDropTables();
            alert("Database Cleared");
        }
    }
    catch(e){
        alert(e);
    }
}